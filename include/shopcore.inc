#if defined _shopcore_include_
  #endinput
#endif
#define _shopcore_include_
 
/*
	Add points to player
*/
native void Shop_Points_Add(int client, int points);

native int Shop_Points_Get(int client);

native void Shop_Points_Take(int client, int points);

native void Shop_Set_Prefix(int client, char[] prefix);

native void Shop_Set_Skin(int client, int number);

native int Shop_Gifts_Create();

native void Shop_Set_Colors(int client, int colors[3]);

native void Shop_Set_Finish(int client, int bFinished);

native int Shop_GetEscape();