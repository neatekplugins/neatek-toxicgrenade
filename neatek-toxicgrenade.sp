#include <sdktools>
#include <smlib>
#include <zombiereloaded>

float g_grenades[64][3];
/*int g_grenades_index[64];*/

#include "functions.sp"
//#include "shopcore/class_entity.sp"

ConVar health_damage;
ConVar grenade_time;

public Plugin myinfo =
{
	name = "NEATEK TOXIC SMOKE",
	author = "NEATEK",
	description = "TOXIC SMOKE",
	version = "1.0.0",
	url = "http://neatek.ru"
};

public void OnPluginStart() 
{
	CreateConVar("sm_plugin_author", "neatek", "Author: Neatek, www.neatek.ru", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	CreateConVar("sm_neatek_toxic_grenade", "1.0.0", "Toxic grenade. Author: Neatek, www.neatek.ru", FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY );
	HookEvent("smokegrenade_detonate", 	smokegrenade_detonate, EventHookMode_Pre);
	//AddTempEntHook("EffectDispatch", TE_EffectDispatch);
	//AddTempEntHook("EffectDispatch", TE_EffectDispatch);
	CreateTimer(1.0, Timer_CheckPlayers, _, TIMER_REPEAT);
	health_damage = CreateConVar("sm_toxic_damage", "25.0", "Damage of toxic grenade");
	grenade_time = CreateConVar("sm_toxic_time", "15.0", "Time of toxic grenade");
	AutoExecConfig(true,"toxic_grenade");
}

public void OnPluginEnd()
{
	//RemoveTempEntHook("EffectDispatch", TE_EffectDispatch);
}

public void hurt_player(int client, int hurt) {
	if(IsCorrectPlayer(client)) {
		int hp = GetClientHealth(client);
		if(hp > hurt) {
			SetEntityHealth(client, (hp-hurt));
			IgniteEntity(client, 1.0);
		}
		else {
			ForcePlayerSuicide(client);
		}
	}
}

public Action Timer_CheckPlayers(Handle timer)
{
	for(int i = 0; i<=MaxClients;i++) {
		if(IsCorrectPlayer(i)) {
			if(IsPlayerAlive(i)) {
				if(player_in_toxic(i) && ZR_IsClientZombie(i)) {
					//PrintToChat(i,"You in toxic!!!");
					//SlapPlayer(i, 25, false);
					hurt_player(i, GetConVarInt(health_damage));
				}
			}
		}
	}

	return Plugin_Continue;
}

public int find_free_index() {
	for(int i = 0 ; i < 64; i++ ) 
	{
		if(g_grenades[i][0] == 0.0 && g_grenades[i][1] == 0.0 && g_grenades[i][2] == 0.0) {
			return i;
		}
	}

	return -1;
}

public bool player_in_toxic(int client) {
	float origin[3];
	float distance = 0.0;
	GetClientAbsOrigin(client, origin);
	for(int i = 0 ; i < 64; i++ ) 
	{
		if(g_grenades[i][0] != 0.0 && g_grenades[i][1] != 0.0 && g_grenades[i][2] != 0.0) {
			distance = GetVectorDistance(origin, g_grenades[i], true);
			
			if(distance < 19000.0) {
				//PrintToChatAll("%f",distance);
				return true;
			}
		}
	}

	return false;

}

public int reset_index(int i) {
	g_grenades[i][0] = 0.0;
	g_grenades[i][1] = 0.0;
	g_grenades[i][2] = 0.0;
	//g_grenades_index[i] = 0;
}

public int push_grenade(float pos[3]) {
	int free_index = find_free_index();
	if(free_index > -1) {
		g_grenades[free_index] = pos;
		//g_grenades_index[free_index] = grenade;
		return free_index;
	}

	return -1;
}

public void OnMapStart() {
	AddFileToDownloadsTable("materials/models/props/slow/smoke_effect/slow_smoke_effect.vmt");
	AddFileToDownloadsTable("materials/models/props/slow/smoke_effect/slow_smoke_effect.vtf");
	AddFileToDownloadsTable("materials/models/props/slow/smoke_effect/slow_smoke_effect_2.vmt");
	AddFileToDownloadsTable("materials/models/props/slow/smoke_effect/slow_smoke_effect_2.vtf");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.dx80.vtx");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.dx90.vtx");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.mdl");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.sw.vtx");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.vvd");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.xbox.vtx");
	AddFileToDownloadsTable("models/props/slow/smoke_effect/slow_smoke_effect.xbox.vtx");
	// 
/*	AddFileToDownloadsTable("particles/smoke_colors_lime.pcf");
	PrecacheGeneric("particles/smoke_colors_lime.pcf", true);
	PrecacheEffect("ParticleEffect");
	PrecacheParticleEffect("explosion_smokegrenade_base_lime");*/

/*	AddFileToDownloadsTable("particle/particle_smokegrenade_thick.vtf");
	AddFileToDownloadsTable("particle/particle_smokegrenade1.vmt");
	PrecacheModel("particle/particle_smokegrenade1.vmt");*/
	PrecacheModel("particle/SmokeStack.vmt");
}


/*public Action TE_EffectDispatch(const char[] te_name, const int[] Players, int numClients, float delay)
{
	int s_EffectName = TE_ReadNum("m_iEffectName");
	//int s_ParticleType = TE_ReadNum("m_nHitBox");

	float s_Origin[3];
	s_Origin[0] = TE_ReadFloat("m_vOrigin.x");
	s_Origin[1] = TE_ReadFloat("m_vOrigin.y");
	s_Origin[2] = TE_ReadFloat("m_vOrigin.z");

	float s_Angle[3];
	TE_ReadVector("m_vAngles", s_Angle);

	int nHitBox = TE_ReadNum("m_nHitBox");

	char sEffectName[64];
	GetEffectName(s_EffectName, sEffectName, sizeof(sEffectName));
	char sParticleEffectName[64];
	GetParticleEffectName(nHitBox, sParticleEffectName, sizeof(sParticleEffectName));

	PrintToChatAll("%s" , sEffectName);
	PrintToChatAll("%s" , sParticleEffectName);

	return Plugin_Continue;
} */

stock void TE_SetupEffect_SmokeColors(int client, const float endpos[3], const float startpos[3], int entindex, char[] effect)
{
	TE_Start("EffectDispatch");

	TE_WriteNum("m_nHitBox", GetParticleEffectIndex(effect));
	
	TE_WriteFloatArray("m_vOrigin.x", endpos, 3);
	TE_WriteFloatArray("m_vStart.x", startpos, 3);
	TE_WriteNum("entindex", entindex);
	
	TE_WriteNum("m_fFlags", 0);
	TE_WriteNum("m_nAttachmentIndex", 0);
	
	TE_WriteNum("m_iEffectName", GetEffectIndex("ParticleEffect"));
		
	TE_SendToClient(client);
}

stock DispatchParticleEffectToAll(p_ParticleType, const Float:p_Origin[3], const Float:p_Angle[3], p_Parent = INVALID_ENT_REFERENCE, Float:p_Delay = 0.0)
{
    TE_Start("EffectDispatch");
    
    TE_WriteNum("m_nHitBox", p_ParticleType);
    TE_WriteFloat("m_vOrigin.x", p_Origin[0]);
    TE_WriteFloat("m_vOrigin.y", p_Origin[1]);
    TE_WriteFloat("m_vOrigin.z", p_Origin[2]);
    TE_WriteFloat("m_vStart.x", p_Origin[0]);
    TE_WriteFloat("m_vStart.y", p_Origin[1]);
    TE_WriteFloat("m_vStart.z", p_Origin[2]);
    TE_WriteVector("m_vAngles", p_Angle);
    
    if(p_Parent == INVALID_ENT_REFERENCE)
        TE_WriteNum("entindex", 0);
    else
        TE_WriteNum("entindex", p_Parent);
        
    TE_SendToAll(p_Delay);
}



public void smokegrenade_detonate(Event event, char[] name, bool dontBroadcast)
{


	float pos[3], projectile_origin[3];
	pos[0] = GetEventFloat(event, "x", 0.0);
	pos[1] = GetEventFloat(event, "y", 0.0);
	pos[2] = GetEventFloat(event, "z", 0.0);

	if(pos[0] != 0.0 && pos[1] != 0.0 && pos[2] != 0.0) {
		int index;
		while((index = FindEntityByClassname(index, "smokegrenade_projectile")) != -1)
		{
			GetEntPropVector(index, Prop_Send, "m_vecOrigin", projectile_origin);
			
			if(pos[0] == projectile_origin[0] && pos[1] == projectile_origin[1] && pos[2] == projectile_origin[2])
			{
				//g_smoke = index;

				int ent = CreateEntityByName("prop_dynamic_override", -1);
				if(ent != -1) {
					SetEntProp(ent, Prop_Send, "m_usSolidFlags", 12);
					SetEntProp(ent, Prop_Data, "m_nSolidType", 6);
					SetEntProp(ent, Prop_Send, "m_CollisionGroup", 1);
					DispatchKeyValue(ent, "model", "models/props/slow/smoke_effect/slow_smoke_effect.mdl"); 
					SetEntProp(ent, Prop_Send, "m_nSequence", 5);
					
					DispatchSpawn(ent);
					
					SetVariantString("spin"); 
					AcceptEntityInput(ent, "SetAnimation", -1, -1, 0);  
					SetEntityMoveType(ent, MOVETYPE_VPHYSICS);

					TeleportEntity(ent, pos, NULL_VECTOR, NULL_VECTOR);

					
					AcceptEntityInput(index, "Kill");

					//int particle = GetEffectIndex("explosion_smokegrenade_base_lime");
					//DispatchParticleEffectToAll(particle, pos, NULL_VECTOR, INVALID_ENT_REFERENCE, 10.0)

					
					//AcceptEntityInput(index, "Kill");

					
					new smoke = CreateEntityByName("env_smokestack");
					if (smoke < 1)
					{
						LogError("env_smokestack error");
						return;
					}

					int indexx = push_grenade(pos);
					DispatchKeyValueVector(smoke, "origin", pos);
					DispatchKeyValue(smoke, "BaseSpread", "100");
					DispatchKeyValue(smoke, "SpreadSpeed", "70");
					DispatchKeyValue(smoke, "Speed", "80");
					DispatchKeyValue(smoke, "StartSize", "100");
					DispatchKeyValue(smoke, "EndSize", "2");
					DispatchKeyValue(smoke, "Rate", "30");
					DispatchKeyValue(smoke, "JetLength", "400");
					DispatchKeyValue(smoke, "Twist", "20"); 
					DispatchKeyValue(smoke, "RenderColor", "166, 237, 78"); 
					DispatchKeyValue(smoke, "RenderAmt", "255");
					DispatchKeyValue(smoke, "SmokeMaterial", "particle/SmokeStack.vmt");
					DispatchSpawn(smoke);
					AcceptEntityInput(smoke, "TurnOn");

/*					decl String:Info[65];
					Format(Info, 65, "OnUser1 !self:kill::%f:1", 15.0);
					SetVariantString(Info); 
					AcceptEntityInput(smoke, "AddOutput"); 
					AcceptEntityInput(smoke, "FireUser1");*/
					//AcceptEntityInput(smoke, "TurnOn");
					DataPack pack = new DataPack();
					pack.WriteCell(smoke);
					pack.WriteCell(ent);
					pack.WriteCell(indexx);
					CreateTimer(GetConVarFloat(grenade_time), Timer_DeleteModel, pack);
				}

				break;
			}
		}
	}
}

public Action Timer_DeleteModel(Handle timer, DataPack pack)
{	
	pack.Reset();
	int smoke = pack.ReadCell();
	int ent = pack.ReadCell();
	int index = pack.ReadCell();
	//CloseHandle(pack);
	pack.Close();

	if(Valid_Entity(smoke)) {
		AcceptEntityInput(smoke, "Kill");
	}
	if(Valid_Entity(ent)) {
		AcceptEntityInput(ent, "Kill");
	}

	reset_index(index);

}

public Action:DeleteParticle(Handle:timer, any:particle)
{
    if (IsValidEntity(particle))
    {
        new String:classN[64];
        GetEdictClassname(particle, classN, sizeof(classN));
        if (StrEqual(classN, "info_particle_system", false))
        {
            RemoveEdict(particle);
        }
    }
}

stock CreateParticle(ent, String:particleType[], Float:time)
{
	new particle = CreateEntityByName("info_particle_system");
	decl String:name[64];
	if (IsValidEdict(particle))
	{
	    new Float:position[3];
	    GetEntPropVector(ent, Prop_Send, "m_vecOrigin", position);
	    TeleportEntity(particle, position, NULL_VECTOR, NULL_VECTOR);
	    GetEntPropString(ent, Prop_Data, "m_iName", name, sizeof(name));
	    DispatchKeyValue(particle, "targetname", "tf2particle");
	    DispatchKeyValue(particle, "parentname", name);
	    DispatchKeyValue(particle, "effect_name", particleType);
	    DispatchSpawn(particle);
	    SetVariantString(name);
	    AcceptEntityInput(particle, "SetParent", particle, particle, 0);
	    ActivateEntity(particle);
	    AcceptEntityInput(particle, "start");
	    CreateTimer(time, DeleteParticle, particle);
	}
}

stock void PrecacheEffect(const char[] sEffectName)
{
    static int table = INVALID_STRING_TABLE;
    
    if (table == INVALID_STRING_TABLE)
    {
        table = FindStringTable("EffectDispatch");
    }
    bool save = LockStringTables(false);
    AddToStringTable(table, sEffectName);
    LockStringTables(save);
}
stock void PrecacheParticleEffect(const char[] sEffectName)
{
    static int table = INVALID_STRING_TABLE;
    
    if (table == INVALID_STRING_TABLE)
    {
        table = FindStringTable("ParticleEffectNames");
    }
    bool save = LockStringTables(false);
    AddToStringTable(table, sEffectName);
    LockStringTables(save);
}  


stock GetEffectIndex(const String:sEffectName[])
{
	static table = INVALID_STRING_TABLE;
	
	if (table == INVALID_STRING_TABLE)
	{
		table = FindStringTable("EffectDispatch");
	}
	
	new iIndex = FindStringIndex(table, sEffectName);
	if(iIndex != INVALID_STRING_INDEX)
		return iIndex;
	
	// This is the invalid string index
	return 0;
}



stock int GetParticleEffectIndex(const char[] sEffectName)
{
	static int table = INVALID_STRING_TABLE;
	
	if (table == INVALID_STRING_TABLE)
	{
		table = FindStringTable("ParticleEffectNames");
	}
	
	int iIndex = FindStringIndex(table, sEffectName);
	if(iIndex != INVALID_STRING_INDEX)
		return iIndex;
	
	// This is the invalid string index
	return 0;
}

stock GetEffectName(index, String:ef_n[], maxlen) 
{
	static table = INVALID_STRING_TABLE;
	if (table == INVALID_STRING_TABLE)
		table = FindStringTable("EffectDispatch"); 
	
	ReadStringTable(table, index, ef_n, maxlen);
}

stock GetParticleEffectName(index, String:ef_n[], maxlen) 
{
	static table = INVALID_STRING_TABLE; 
	if (table == INVALID_STRING_TABLE)  {
		table = FindStringTable("ParticleEffectNames"); 
	}

	ReadStringTable(table, index, ef_n, maxlen);
}

/*stock int PrecacheParticleSystem(const char[] particleSystem)
{
    static int particleEffectNames = INVALID_STRING_TABLE;

    if (particleEffectNames == INVALID_STRING_TABLE) {
        if ((particleEffectNames = FindStringTable("ParticleEffectNames")) == INVALID_STRING_TABLE) {
            return INVALID_STRING_INDEX;
        }
    }

    int index = FindStringIndex2(particleEffectNames, particleSystem);
    if (index == INVALID_STRING_INDEX) {
        int numStrings = GetStringTableNumStrings(particleEffectNames);
        if (numStrings >= GetStringTableMaxStrings(particleEffectNames)) {
            return INVALID_STRING_INDEX;
        }
        
        AddToStringTable(particleEffectNames, particleSystem);
        index = numStrings;
    }
    
    return index;
}

stock int FindStringIndex2(int tableidx, const char[] str)
{
    char buf[1024];
    
    int numStrings = GetStringTableNumStrings(tableidx);
    for (int i=0; i < numStrings; i++) {
        ReadStringTable(tableidx, i, buf, sizeof(buf));
        
        if (StrEqual(buf, str)) {
            return i;
        }
    }
    
    return INVALID_STRING_INDEX;
}*/