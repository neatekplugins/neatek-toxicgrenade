// Function for check is valid or not player
public bool IsCorrectPlayer(int client) {
	if(client > 4096) {
		client = EntRefToEntIndex(client);
	}
		
	if( (client < 1 || client > MaxClients) || !IsClientConnected(client) ||  !IsClientInGame( client ) ) {
		return false;
	}
	
	#if !defined PLUGIN_DEBUG_MODE
	if(IsFakeClient(client) || IsClientSourceTV(client)) {
		return false;
	}
	#endif
	
	return true;
}

// Удалить все оружия
public void StripWeapons(int client) {
	if(IsCorrectPlayer(client)) 
		return;

	if(!IsPlayerAlive(client))
		return;

	for(int i = 0; i <= 6; i++) {
		StripWeaponSlot(client, i);
	}
}

// Удалить слот оружия
public void StripWeaponSlot(int client, int slot) {
	if(IsCorrectPlayer(client)) 
		return;

	if(!IsPlayerAlive(client))
		return;

	int wpn = GetPlayerWeaponSlot(client, slot);
	if(wpn != -1) {

		RemovePlayerItem( client, wpn )
		
		if(IsValidEntity(wpn) && wpn != -1 ) {
			AcceptEntityInput(wpn, "Kill");
		}
	}
}

// Дать ножик игроку если его нету
public bool GiveKnife(int client) {
	if(IsCorrectPlayer(client)) 
		return false;
	
	int wpn = GetPlayerWeaponSlot(client, 2);
	
	if(wpn == -1) {
		GivePlayerItem(client, "weapon_knife");
		return true;
	}

	return true;
}

// Найти Entity бомбы
public int find_the_bomb()
{
	return FindEntityByClassname(-1, "weapon_c4");
}

// Найти клиента у кого бомба
public int find_client_bomb() {
	int wpn = -1;
	for(int i=0;i<=MaxClients;i++) {
		if(IsCorrectPlayer(i)) {
			wpn = GetPlayerWeaponSlot(i, 4);
			if(wpn > 0) {
				return i;
			}
		}
	}

	return -1;
}

public bool Valid_Entity(int ent) {
	// && Entity_IsValid(ent)
	if(IsValidEntity(ent)) {
		return true;
	}

	return false;
}